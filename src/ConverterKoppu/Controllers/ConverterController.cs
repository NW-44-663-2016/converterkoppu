﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using ConverterKoppu.Models;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace ConverterKoppu.Controllers
{
    public class ConverterController : Controller
    {
        public IActionResult Koppu()
        {
            ViewData["Title"] = "Converter App by Koppu";
            ViewData["Result"] = "";

            Converter converter = new Converter();
            return View(converter);
        }

        public IActionResult Convert(Converter converter)
        {
            if (ModelState.IsValid)
            {
                ViewData["Title"] = "Converted by Koppu";
                ViewData["Value"] = (int)((converter.Temperature_F - 32) * 5.0 / 9.0);
                ViewData["Result"] = "Temperature in C = " +
                    (int)((converter.Temperature_F - 32) * 5.0 / 9.0);
                
            }
            return View("Koppu", converter);
        }
    }
}
